package view;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class SimulatorFrame extends JFrame{
	private static final long serialVersionUID = 1L;
	private JPanel panel;
	private JButton start;
	private JLabel minProcTimeL,maxProcTimeL,minNoOfTasksL,maxNoOfTasksL,noOfQueuesL,startL,endL;
	private JTextField minProcTimeT,maxProcTimeT,minNoOfTasksT,maxNoOfTasksT,noOfQueuesT,startT,endT;
	private JTextArea statistics,activity;
	private boolean isRunning;
	
	public SimulatorFrame(){
		super("Queues Simulation");
		isRunning = true;
		panel = new JPanel();
		panel.setLayout(new GridLayout(0,2));
		start = new JButton("START");
		minProcTimeL = new JLabel("minProcTime");
		maxProcTimeL = new JLabel("maxProcTime");
		minNoOfTasksL = new JLabel("minNoOfTasks");
		maxNoOfTasksL = new JLabel("maxNoOfTasks");
		noOfQueuesL = new JLabel("noOfQueues");
		startL = new JLabel("startTime");
		endL = new JLabel("endTime");
		minProcTimeT = new JTextField("2");
		maxProcTimeT = new JTextField("10");
		minNoOfTasksT = new JTextField("40");
		maxNoOfTasksT = new JTextField("60");
		noOfQueuesT = new JTextField("3");
		startT = new JTextField("8");
		endT = new JTextField("9");
		statistics = new JTextArea();
		activity = new JTextArea();
		JScrollPane scrollPane = new JScrollPane(activity);
		
		JPanel leftPanel = new JPanel();
		leftPanel.setLayout(new BoxLayout(leftPanel,BoxLayout.Y_AXIS));
		JPanel procPanel = new JPanel();
		procPanel.setLayout(new BoxLayout(procPanel,BoxLayout.X_AXIS));
		procPanel.add(this.minProcTimeL);
		procPanel.add(this.minProcTimeT);
		procPanel.add(this.maxProcTimeL);
		procPanel.add(this.maxProcTimeT);
		JPanel taskPanel = new JPanel();
		taskPanel.setLayout(new BoxLayout(taskPanel,BoxLayout.X_AXIS));
		taskPanel.add(this.minNoOfTasksL);
		taskPanel.add(this.minNoOfTasksT);
		taskPanel.add(this.maxNoOfTasksL);
		taskPanel.add(this.maxNoOfTasksT);
		JPanel queuePanel = new JPanel();
		queuePanel.setLayout(new BoxLayout(queuePanel,BoxLayout.X_AXIS));
		queuePanel.add(this.noOfQueuesL);
		queuePanel.add(this.noOfQueuesT);
		JPanel timePanel = new JPanel();
		timePanel.setLayout(new BoxLayout(timePanel,BoxLayout.X_AXIS));
		timePanel.add(this.startL);
		timePanel.add(this.startT);
		timePanel.add(this.endL);
		timePanel.add(this.endT);
		leftPanel.add(procPanel);
		leftPanel.add(Box.createRigidArea(new Dimension(0,30)));
		leftPanel.add(taskPanel);
		leftPanel.add(Box.createRigidArea(new Dimension(0,30)));
		leftPanel.add(queuePanel);
		leftPanel.add(Box.createRigidArea(new Dimension(0,30)));
		leftPanel.add(timePanel);
		leftPanel.add(Box.createRigidArea(new Dimension(0,30)));
		leftPanel.add(start);
		leftPanel.add(Box.createRigidArea(new Dimension(0,300)));
		
		JPanel rightPanel = new JPanel();
		rightPanel.setLayout(new GridLayout(2,0));
		rightPanel.add(scrollPane);
		rightPanel.add(this.statistics);
		
		panel.add(leftPanel);
		panel.add(rightPanel);
		
		
		this.add(panel);
		this.setSize(1200,600);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
	}
	public void setStartActionListener(ActionListener listener){
		start.addActionListener(listener);
	}
	public JTextField getMinProcTimeT() {
		return minProcTimeT;
	}

	public JTextField getMaxProcTimeT() {
		return maxProcTimeT;
	}

	public JTextField getMinNoOfTasksT() {
		return minNoOfTasksT;
	}

	public JTextField getMaxNoOfTasksT() {
		return maxNoOfTasksT;
	}

	public JTextField getNoOfQueuesT() {
		return noOfQueuesT;
	}

	public JTextField getStartT() {
		return startT;
	}

	public JTextField getEndT() {
		return endT;
	}

	public JTextArea getStatistics() {
		return statistics;
	}

	public JTextArea getActivity() {
		return activity;
	}
	public boolean isRunning() {
		return isRunning;
	}
	public void setRunning(boolean isRunning) {
		this.isRunning = isRunning;
	}
	
	
}

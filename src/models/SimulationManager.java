package models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import javax.swing.JTextArea;

import view.SimulatorFrame;

public class SimulationManager implements Runnable{
	public int startTime,endTime;
	public int maxProcessingTime;
	public int minProcessingTime;
	public int numberOfServers;
	public int minNumberOfTasks;
	public int maxNumberOfTasks;
	public int peakHour,peakValue;
	public SelectionPolicy selectionPolicy = SelectionPolicy.SHORTEST_TIME;
	private Scheduler scheduler;
	private List<Task> generatedTasks;
	private Thread[] myThreads;	
	private JTextArea act,stats;
	public SimulationManager(){
		peakHour = 0;
		peakValue = 0;
		generatedTasks = new ArrayList<Task>();
	}
	private void generateNRandomTasks(){
		//generate N randomTasks
		int randomNum = ThreadLocalRandom.current().nextInt(minNumberOfTasks, maxNumberOfTasks + 1);
		for (int i=1;i<=randomNum;i++){
			//generate random processing and arrival time
			int randProc = ThreadLocalRandom.current().nextInt(minProcessingTime, maxProcessingTime);
			int randArr = ThreadLocalRandom.current().nextInt(1,( endTime - startTime ) * 60 + 1);
			Task task = new Task(randProc,randArr);
			generatedTasks.add(task);
		}
		//sort the list with respect to arrivalTime
		Collections.sort(generatedTasks,new CompareByArrivalTime()); 
		//act.append(String.format("%d tasks were generated", randomNum));
		//act.append("\n");
	}
	public void run(){
		//initiate the variables that use informations from gui
		int currentTime = 0;
		int timeLimit = ( endTime - startTime ) * 60;
		scheduler = new Scheduler(numberOfServers,stats,act);
		scheduler.changeStrategy(selectionPolicy);
		myThreads = new Thread[numberOfServers];
		for (int i=0;i<numberOfServers;i++){
			myThreads[i] = new Thread(scheduler.getServers().get(i));
			String name = String.format("Queue %d", i);
			myThreads[i].setName(name);
		}
		generateNRandomTasks();
		for (int i=0;i<numberOfServers;i++)
			myThreads[i].start();	
		while (currentTime < timeLimit){
			//iterate generatedTasks list and pick the tasks with the corresponding arrival time
			if(!generatedTasks.isEmpty())
				while (generatedTasks.get(0).getArrivalTime() == currentTime){
					int i = scheduler.dispatchTask(generatedTasks.get(0));//send task to queue
					//format the time
					int d = timeLimit - generatedTasks.get(0).getArrivalTime();
					int e = 0;
					while (d > 60){
						d-=60;
						e++;
					}
					act.append(String.format("Task enters the server no. %d , arr time:%d:%d, proc time:%ds \n",i,startTime+e,60-d,generatedTasks.get(0).getProcessingTime()));
					act.append("\n");
					//delete task from list
					generatedTasks.remove(0);
					//find the peak hour
					if (peakValue < avaibleTasks()){
						peakValue = avaibleTasks();
						peakHour = currentTime;
					}
					if (generatedTasks.isEmpty())
						break;
				}
			currentTime++;
			if (currentTime >= timeLimit){
				//close the queues when the time is up
				for (Server s : scheduler.getServers())
					s.setRunning(false);
				stats.append("Time expired.Waiting for the remaining task to be processed:");
				stats.append("\n");
				int a = timeLimit - peakHour;
				int b = 0;
				while (a > 60){
					a-=60;
					b++;
				}
				stats.append(String.format("Peak hour:%d:%d", startTime+b , 60-a));
				stats.append("\n");
			}	
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	//method that return the total number of tasks in the queues
	public int avaibleTasks(){
		int num = 0;
		for (Server s : scheduler.getServers())
			for (Task t : s.getTasks())
				num++;
		return num;
	}
	public void setSimulationParameters(int minProc,int maxProc,int minTask,int maxTask,int queNo,int startTime,int endTime){
		this.minProcessingTime = minProc;
		this.maxProcessingTime = maxProc;
		this.minNumberOfTasks = minTask;
		this.maxNumberOfTasks = maxTask;
		this.numberOfServers = queNo;
		this.startTime = startTime;
		this.endTime = endTime;
	}
	public void setTexts(JTextArea up,JTextArea down){
		this.act = up;
		this.stats = down;
	}
}

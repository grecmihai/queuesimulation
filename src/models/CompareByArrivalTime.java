package models;

import java.util.Comparator;

public class CompareByArrivalTime implements Comparator<Task>{
	@Override
	public int compare(Task t1, Task t2){
		int result;
		result = t1.getArrivalTime() - t2.getArrivalTime();
		return result;
	}
}

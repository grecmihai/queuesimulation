package models;

import controller.QueueController;
import view.SimulatorFrame;

public class Simulation {

	public static void main(String[] args) {
		SimulationManager manager = new SimulationManager();
		SimulatorFrame frame = new SimulatorFrame();
		QueueController controller = new QueueController(manager,frame);
		frame.setVisible(true);
	}

}

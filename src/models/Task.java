package models;

public class Task {
	private int waitingTime;
	private int processingTime;
	private int arrivalTime;
	
	public Task(int pT, int aT){
		processingTime = pT;
		arrivalTime = aT;
	}	
	public int finalTime(){
		return waitingTime + processingTime;
	}
	public int getProcessingTime() {
		return processingTime;
	}
	public int getWaitingTime() {
		return waitingTime;
	}
	public void setWaitingTime(int waitingTime) {
		this.waitingTime = waitingTime;
	}
	public int getArrivalTime() {
		return arrivalTime;
	}
	
	
}

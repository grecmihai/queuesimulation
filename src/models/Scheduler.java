
package models;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JTextArea;

public class Scheduler {
	private List<Server> servers;
	private int maxNoServers;
	private Strategy strategy;
	
	public Scheduler(int maxNoServers,JTextArea area,JTextArea area2){
		this.maxNoServers = maxNoServers;
		servers = new ArrayList<Server>();
		for (int i=0;i<this.maxNoServers;i++)
			servers.add(new Server(String.format("Q%d", i),area,area2));
		strategy = new ConcreteStrategyTime();
	}
	public void changeStrategy(SelectionPolicy policy){
		if (policy == SelectionPolicy.SHORTEST_QUEUE)
			strategy = new ConcreteStrategyQueue();
		if (policy == SelectionPolicy.SHORTEST_TIME)
			strategy = new ConcreteStrategyTime();
	}
	public int dispatchTask(Task t){
		int i = strategy.addTask(servers, t);
		return i;
	}
	public List<Server> getServers() {
		return servers;
	}
	
}

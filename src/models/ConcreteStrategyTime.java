package models;

import java.util.List;

public class ConcreteStrategyTime implements Strategy{
	public int addTask(List<Server> servers,Task t){
		int i = -1;
		int pos = 0;
		int min = servers.get(pos).getWaitingPeriod().get();
		for (Server server : servers){
			i++;
			if (server.getWaitingPeriod().get() < min){
				pos = i;
				min = server.getWaitingPeriod().get();
			}
		}
		servers.get(pos).addTask(t);
		return pos;
	}
}

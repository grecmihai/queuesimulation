package models;

import java.util.List;

public class ConcreteStrategyQueue implements Strategy{
	public int addTask(List<Server> servers,Task t){
		int i = 0;
		int min = servers.get(0).getTasks().size();
		for (Server server: servers){
			if (server.getTasks().size() < min){
			i++;
			min = server.getTasks().size();
			}
		}
		servers.get(i).addTask(t);
		return i;
	}
}

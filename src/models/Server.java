package models;

import java.util.List;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicInteger;

import javax.swing.JTextArea;

public class Server implements Runnable{
	private List<Task> tasks;
	private AtomicInteger waitingPeriod;
	private AtomicInteger totalNumOfTasks, totalWaitingPeriod, totalServiceTime,emptyTime;//used for final statistics
	private boolean running;
	private JTextArea stats,med;
	private String name;
	public Server(String name,JTextArea stats,JTextArea med){
		this.stats = stats;
		this.med = med;
		this.name = name;
		tasks = new Vector<Task>();
		waitingPeriod = new AtomicInteger(0);
		totalNumOfTasks = new AtomicInteger(0);
		totalWaitingPeriod = new AtomicInteger(0);
		totalServiceTime = new AtomicInteger(0);
		emptyTime = new AtomicInteger(0);
		running = true;
	}
	public void addTask(Task newTask){
		//add the task to the queue and set the proper waiting time
		newTask.setWaitingTime(waitingPeriod.get());
		tasks.add(newTask);
		//add task's waiting period to the general one
		waitingPeriod.addAndGet(newTask.getProcessingTime());
	}
	public void run(){
		while(running){
			if (!tasks.isEmpty()){
				try {
					totalNumOfTasks.incrementAndGet();//increment the queue's number of tasks
					//get the next task from queue
					Task endTask = tasks.remove(0);
					//add task's waiting and processing one to the total ones
					totalWaitingPeriod.addAndGet(endTask.getWaitingTime());
					totalServiceTime.addAndGet(endTask.getProcessingTime());
					//put the thread to sleep for a time equal with the task's processing time
					Thread.sleep(endTask.getProcessingTime()*200);
					med.append(String.format("Task from %s was executed", this.name));
					med.append("\n");
					//decrement the queue waiting period
					waitingPeriod.set(waitingPeriod.get() - endTask.getProcessingTime());
					if (!running){
						//when the last task is executed, show queue's statistics
						stats.append(String.format("%s:avgWaitingTime:%.2fs    avgProcessingTime:%.2fs    emptyTime:%ds",this.name,this.avgWaitingTime(),this.avgServiceTime(),this.emptyTime.get()));
						stats.append("\n");
						int dead = 0;
						while (!tasks.isEmpty()){
							dead++;
							tasks.remove(0);
						}
						stats.append(String.format("%s:%d tasks unserviced", this.name,dead));
						stats.append("\n");
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else
				try {
					//case when the queue is empty, the queue is put to sleep until some task comes
					
					emptyTime.incrementAndGet();
					Thread.sleep(200);
					if (!running){
						//when the last task is executed, show queue's statistics
						if (this.totalNumOfTasks.get() == 0){
							stats.append(String.format("%s was not used.Empty time=%d", this.name,this.emptyTime.get()));
							stats.append("\n");
						}
					
						else{
						stats.append(String.format("%s:avgWaitingTime:%.2fs    avgProcessingTime:%.2fs    emptyTime:%ds",this.name,this.avgWaitingTime(),this.avgServiceTime(),this.emptyTime.get()));
						stats.append("\n");
						}
						stats.append(String.format("%s:0 tasks unserviced", this.name));
						stats.append("\n");
					}
					
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
	}
	public AtomicInteger getWaitingPeriod() {
		return waitingPeriod;
	}
	public List<Task> getTasks() {
		return tasks;
	}
	public synchronized float avgWaitingTime(){
		return (float) this.totalWaitingPeriod.get() / this.totalNumOfTasks.get();
	}
	public synchronized float avgServiceTime(){
		return (float) this.totalServiceTime.get() / this.totalNumOfTasks.get();
	}
	public void setRunning(boolean running) {
		this.running = running;
	}
	public boolean isRunning() {
		return running;
	}
	public JTextArea getStats() {
		return stats;
	}
	public void setStats(JTextArea stats) {
		this.stats = stats;
	}
	public JTextArea getMed() {
		return med;
	}
	public void setMed(JTextArea med) {
		this.med = med;
	}
	
	
	
	
}

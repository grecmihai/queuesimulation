package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import models.SimulationManager;
import view.SimulatorFrame;

public class QueueController {
	SimulationManager manager;
	SimulatorFrame frame;
	
	public QueueController(SimulationManager m, SimulatorFrame f){
		manager = m;
		frame = f;
		frame.setStartActionListener(new StartActionListener());
	}
	class StartActionListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			if (frame.isRunning()){
				frame.setRunning(false);//block the start button when the system is running
				//verify the values in the user interface and unblock the start button to try again
				if (Integer.parseInt(frame.getMinNoOfTasksT().getText()) > Integer.parseInt(frame.getMaxNoOfTasksT().getText())){
					JOptionPane.showMessageDialog(null, "Please insert a valid interval of tasks","",JOptionPane.PLAIN_MESSAGE);
					frame.setRunning(true);
						try {
							throw new Exception("Illegal task interval");
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				}
				if (Integer.parseInt(frame.getMinProcTimeT().getText()) > Integer.parseInt(frame.getMaxProcTimeT().getText())){
					frame.setRunning(true);
					JOptionPane.showMessageDialog(null, "Please insert a valid interval of processing time","",JOptionPane.PLAIN_MESSAGE);
						try {
							throw new Exception("Illegal processing time interval interval");
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				}
				if (Integer.parseInt(frame.getStartT().getText()) > Integer.parseInt(frame.getEndT().getText())){
					frame.setRunning(true);
					JOptionPane.showMessageDialog(null, "Please insert a valid interval of simulation","",JOptionPane.PLAIN_MESSAGE);
						try {
							throw new Exception("Illegal simulation interval");
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				}
				//initialize SimulationManager variables
				manager.setSimulationParameters(Integer.parseInt(frame.getMinProcTimeT().getText()), Integer.parseInt(frame.getMaxProcTimeT().getText()),
						Integer.parseInt(frame.getMinNoOfTasksT().getText()), Integer.parseInt(frame.getMaxNoOfTasksT().getText()),
						Integer.parseInt(frame.getNoOfQueuesT().getText()),
						Integer.parseInt(frame.getStartT().getText()), Integer.parseInt(frame.getEndT().getText()));
				
				Thread t = new Thread(){
					@Override
					public void run(){
						//constantly update the user interface
						Thread man = new Thread(manager);
						man.start();
						manager.setTexts(frame.getActivity(), frame.getStatistics());
						try {
							sleep(200);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				};
				t.start();
			}
		}
	}
}
